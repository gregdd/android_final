package com.example.gregory.unitconverter;

import com.example.gregory.unitconverter.beans.ConvertValueBean;
import com.example.gregory.unitconverter.services.ConvertValueGenerator;
import com.example.gregory.unitconverter.services.ValueConverter;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ConverterUnitTest {
    @Test
    public void testGenerateDistance() {
        assertTrue(ConvertValueGenerator.generateConverterForDistance().size() > 0);
        assertTrue(ConvertValueGenerator.generateConverterForDistance().get(0).getUom().equals("mm"));
    }

    @Test
    public void testGenerateVolume() {
        assertTrue(ConvertValueGenerator.generateConverterForVolume().size() > 0);
        assertTrue(ConvertValueGenerator.generateConverterForVolume().get(0).getUom().equals("l"));
    }

    @Test
    public void testConvertVolume(){
        ValueConverter converter = new ValueConverter();
        List<ConvertValueBean> list = ConvertValueGenerator.generateConverterForVolume();
        converter.convertValue("cm3", 1000, list);
        assertTrue(list.get(0).getConvertedValue() == 1);
        converter.convertValue("dm3", 10, list);
        assertTrue(list.get(0).getConvertedValue() == 10);
    }

    @Test
    public void testConvertDistance(){
        ValueConverter converter = new ValueConverter();
        List<ConvertValueBean> list = ConvertValueGenerator.generateConverterForDistance();
        converter.convertValue("cm", 1000, list);
        assertTrue(list.get(3).getConvertedValue() == 10); //get 3 est la valeur m
        converter.convertValue("dm", 1000, list);
        assertTrue(list.get(3).getConvertedValue() == 100);
    }

}
package com.example.gregory.unitconverter.services;

import com.example.gregory.unitconverter.beans.ConvertValueBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gregory on 12/12/2016.
 */

public class ConvertValueGenerator {
    public static List<ConvertValueBean> generateConverterForDistance(){
        List<ConvertValueBean> listConvertValueBean = new ArrayList<>();
        listConvertValueBean.add(new ConvertValueBean("mm", "m", 1000));
        listConvertValueBean.add(new ConvertValueBean("cm", "m", 100));
        listConvertValueBean.add(new ConvertValueBean("dm", "m", 10));
        listConvertValueBean.add(new ConvertValueBean("m", "m", 1));
        listConvertValueBean.add(new ConvertValueBean("inch", "m", 39.3701));
        listConvertValueBean.add(new ConvertValueBean("ft", "m", 3.28084));
        listConvertValueBean.add(new ConvertValueBean("yd", "m", 1.09361));
        listConvertValueBean.add(new ConvertValueBean("mile", "m", 0.000621371));
        listConvertValueBean.add(new ConvertValueBean("km", "m", 0.001));
        return listConvertValueBean;
    }

    public static List<ConvertValueBean> generateConverterForVolume(){
        List<ConvertValueBean> listConvertValueBean = new ArrayList<>();
        listConvertValueBean.add(new ConvertValueBean("l", "l", 1));
        listConvertValueBean.add(new ConvertValueBean("mm3", "l", 1000000));
        listConvertValueBean.add(new ConvertValueBean("cm3", "l", 1000));
        listConvertValueBean.add(new ConvertValueBean("dm3", "l", 1));
        listConvertValueBean.add(new ConvertValueBean("m3", "l", 0.001));
        listConvertValueBean.add(new ConvertValueBean("in3", "l", 61.0237));
        listConvertValueBean.add(new ConvertValueBean("ft3", "l", 0.0353147));
        listConvertValueBean.add(new ConvertValueBean("gal (US)", "l", 0.264172));
        listConvertValueBean.add(new ConvertValueBean("gal (UK)", "l", 0.219969));
        listConvertValueBean.add(new ConvertValueBean("fl oz (US)", "l", 33.814));

        return listConvertValueBean;
    }
}

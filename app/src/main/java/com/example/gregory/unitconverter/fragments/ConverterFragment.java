package com.example.gregory.unitconverter.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gregory.unitconverter.R;
import com.example.gregory.unitconverter.adapters.CustomListConverterAdapter;
import com.example.gregory.unitconverter.api.Fetch;
import com.example.gregory.unitconverter.api.FetchInterface;
import com.example.gregory.unitconverter.beans.ConvertValueBean;
import com.example.gregory.unitconverter.services.ConvertValueGenerator;
import com.example.gregory.unitconverter.services.JsonParser;
import com.example.gregory.unitconverter.services.ValueConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gregory on 12/11/2016.
 */

public class ConverterFragment extends Fragment implements FetchInterface {
    private boolean loaded = false;
    private String title = "NO TITLE";
    private String type;
    private List<ConvertValueBean> convertValueBeanList = new ArrayList<>();
    private CustomListConverterAdapter customAdapter;


    private ValueConverter valueConverter;

    private ListView listView;
    private TextView inputValue;
    private Spinner spinner;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.converter_fragment, container, false);
        spinner = (Spinner) view.findViewById(R.id.unit);

        setConvertValueList();

        valueConverter = new ValueConverter();
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                CellFragment frag = new CellFragment();
                frag.setSelectedCell(view);
                frag.setCancelable(false);
                frag.show(manager, "TAG");
            }
        });
        inputValue = (TextView) view.findViewById(R.id.inputValue);
        inputValue.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                refreshList(s);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });
        setAdataper();
        return view;
    }

    private void setAdataper(){
        customAdapter = new CustomListConverterAdapter(getContext());
        for(ConvertValueBean bean : this.convertValueBeanList){
            if (bean.isShow()){
                customAdapter.add(bean);
            }
        }
        listView.setAdapter(customAdapter);
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public List<ConvertValueBean> getConvertValueBeanList() {
        return convertValueBeanList;
    }

    public void setConvertValueBeanList(List<ConvertValueBean> convertValueBeanList) {
        this.convertValueBeanList = convertValueBeanList;
        initSpinner();
        refreshList(inputValue.getText().toString());
    }

    private void setConvertValueList(){
            switch (type) {
                case "DIST":
                    if (!loaded) {
                        this.convertValueBeanList = ConvertValueGenerator.generateConverterForDistance();
                    }
                    loaded = true;
                    initSpinner();
                    break;
                case "VOL":
                    if (!loaded) {
                        this.convertValueBeanList = ConvertValueGenerator.generateConverterForVolume();
                    }
                    loaded = true;
                    initSpinner();
                    break;
                case "CUR":
                    if (!loaded) {
                        Fetch fetch = new Fetch(this);
                        fetch.execute();
                    } else {
                        initSpinner();
                    }
                    break;
            }
    }

    private void refreshList(CharSequence s){
        double input = 0;
        if (!s.toString().equals("")) {
            input = Double.parseDouble(s.toString());
        }

        if (spinner.getSelectedItem() != null) {
            valueConverter.convertValue(spinner.getSelectedItem().toString(), input, convertValueBeanList);
        }
        setAdataper();
    }

    private List<String> loadListUOM(){
        List<String> listUOM = new ArrayList<>();
        for(ConvertValueBean value : this.convertValueBeanList){
            listUOM.add(value.getUom());
        }
        return listUOM;
    }

    private void initSpinner(){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, loadListUOM());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                refreshList(inputValue.getText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void onResponse(String output) {
        if (output != null) {
            Toast.makeText(getContext(), getResources().getString(R.string.apisucces), Toast.LENGTH_SHORT).show();
            JsonParser parser = new JsonParser();
            this.convertValueBeanList = parser.parseRates(output);
            setAdataper();
            initSpinner();
            loaded = true;
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.apierror), Toast.LENGTH_SHORT).show();
        }
    }
}

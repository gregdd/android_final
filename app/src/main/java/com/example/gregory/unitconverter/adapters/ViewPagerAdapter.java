package com.example.gregory.unitconverter.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.gregory.unitconverter.fragments.ConverterFragment;

import java.util.List;

/**
 * Created by Gregory on 12/12/2016.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    List<ConverterFragment> listFragment;

    public ViewPagerAdapter(FragmentManager fm, List<ConverterFragment> listFragment) {
        super(fm);
        this.listFragment = listFragment;
    }

    @Override
    public int getCount() {

        return  listFragment.size();
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return listFragment.get(position).getTitle();
    }
}
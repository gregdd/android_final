package com.example.gregory.unitconverter.services;

import com.example.gregory.unitconverter.beans.ConvertValueBean;

import java.util.List;

/**
 * Created by Gregory on 12/12/2016.
 */

public class ValueConverter {
    //ce convertisseur est generique, il fonctionne pour tout type de conversion donc possibilite d'en ajouter plus sans ajouter des convertisseurs specifique
    //tant qu'on respecte le bean ConvertValueBean
    public void convertValue(String fromUnit, double fromValue, List<ConvertValueBean> listToConvert){
        if (listToConvert.size() > 0) {
            String baseValue = listToConvert.get(0).getBaseValue();
            double valueInBaseValue = fromValue;
            if (!baseValue.equals(fromUnit)) {
                valueInBaseValue = calculateValueToConvertInBaseValue(fromValue, fromUnit, listToConvert);
            }
            convertAllValues(valueInBaseValue, listToConvert);
        }
    }

    //calculer la valeur a convertir vers la valeur de base (ex: 8 cm egale ? en m)
    private double calculateValueToConvertInBaseValue(double fromValue, String fromUnit, List<ConvertValueBean> listToConvert){
        for(ConvertValueBean convertValueBean : listToConvert){
            if (convertValueBean.getUom().equals(fromUnit)){
                return fromValue / convertValueBean.getConvertedValueForOneBaseValue();
            }
        }
        return 0;
    }

    //convertir toutes les valeurs de la liste vers la valeur correspondante
    private void convertAllValues(double valueBase, List<ConvertValueBean> listToConvert){
        for(ConvertValueBean convertValueBean : listToConvert){
            convertValueBean.setConvertedValue(valueBase * convertValueBean.getConvertedValueForOneBaseValue());
        }
    }
}

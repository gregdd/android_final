package com.example.gregory.unitconverter.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.gregory.unitconverter.R;
import com.example.gregory.unitconverter.adapters.ViewPagerAdapter;
import com.example.gregory.unitconverter.beans.ConvertValueBean;
import com.example.gregory.unitconverter.fragments.ConverterFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    static final int ENABLE_DISABLE_ITEMS = 1;

    private List<ConverterFragment> listFragments = new ArrayList<>();
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);

        generateFragments();
        generateViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                launchSettingsActivity();
                return true;

            case R.id.action_about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.dialog).setTitle(R.string.about);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {}
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void launchSettingsActivity(){
        Intent i = new Intent(this, SettingsListActivity.class);
        i.putExtra("list", (ArrayList)listFragments.get(viewPager.getCurrentItem()).getConvertValueBeanList());
        i.putExtra("position", viewPager.getCurrentItem());
        startActivityForResult(i, ENABLE_DISABLE_ITEMS);
    }

    public void generateFragments(){
        generateDistanceFramgment();
        generateVolumeFragment();
        generateCurrencyFragment();
    }

    private void generateVolumeFragment(){
        ConverterFragment fragVol = new ConverterFragment();
        fragVol.setTitle(getResources().getString(R.string.volume));
        fragVol.setType("VOL");
        listFragments.add(fragVol);
    }

    private void generateDistanceFramgment(){
        ConverterFragment fragDist = new ConverterFragment();
        fragDist.setTitle(getResources().getString(R.string.distance));
        fragDist.setType("DIST");
        listFragments.add(fragDist);
    }

    private void generateCurrencyFragment() {
        ConverterFragment fragCurr = new ConverterFragment();
        fragCurr.setTitle(getResources().getString(R.string.currency));
        fragCurr.setType("CUR");
        listFragments.add(fragCurr);
    }

    public void generateViewPager(){
        ViewPagerAdapter adapter =  new ViewPagerAdapter(getSupportFragmentManager(), listFragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ENABLE_DISABLE_ITEMS) {
            if (resultCode == RESULT_OK) {
                List<ConvertValueBean> beans = (ArrayList<ConvertValueBean>)data.getSerializableExtra("list");
                listFragments.get((data.getIntExtra("position", -1))).setConvertValueBeanList(beans);
            }
        }
    }
}

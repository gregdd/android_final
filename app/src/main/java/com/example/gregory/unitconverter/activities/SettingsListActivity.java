package com.example.gregory.unitconverter.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.example.gregory.unitconverter.R;
import com.example.gregory.unitconverter.beans.ConvertValueBean;

import java.util.ArrayList;
import java.util.List;

public class SettingsListActivity extends AppCompatActivity {

    ListView listView;
    List<ConvertValueBean> beans;
    int fragmentPos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_list);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarSettings);
        setSupportActionBar(myToolbar);

        initList();
        initChecked();
        initListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("list", (ArrayList)beans);
                returnIntent.putExtra("position", fragmentPos);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;

            case R.id.action_discard:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initListener(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView checkedTextView = ((CheckedTextView)view);
                beans.get(position).setShow(checkedTextView.isChecked());
            }
        });
    }

    private void initChecked(){
        fragmentPos = getIntent().getIntExtra("position", -1);
        for (int i = 0; i < listView.getCount(); i++) {
            listView.setItemChecked(i, beans.get(i).isShow());
        }
    }

    private void initList(){
        listView = (ListView) findViewById(R.id.listViewSettings);
        beans = (ArrayList<ConvertValueBean>)getIntent().getSerializableExtra("list");
        List<String> stringUOM = new ArrayList<>();
        for(ConvertValueBean bean : beans){
            stringUOM.add(bean.getUom());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, stringUOM);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }
}

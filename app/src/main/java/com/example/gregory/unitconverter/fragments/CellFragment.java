package com.example.gregory.unitconverter.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.gregory.unitconverter.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Gregory on 12/13/2016.
 */

public class CellFragment extends DialogFragment {
    private View selectedCell;
    private Spinner colorSpinner;
    private Button resetBtn;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.cell_fragment, null);
        colorSpinner = (Spinner) v.findViewById(R.id.colorSpinner);
        resetBtn = (Button) v.findViewById(R.id.button);
        resetBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                selectedCell.setBackgroundColor(stringToColor("reset"));
            }
        });
        initSpinner();
        return new AlertDialog.Builder(getActivity()).setTitle(R.string.celloptions).setPositiveButton(R.string.save, null).setView(v).create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void setSelectedCell(View v){
        selectedCell = v;
    }

    private void initSpinner(){
        final List<String> listColors = Arrays.asList(getString(R.string.blue), getString(R.string.green), getString(R.string.yellow));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, listColors);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSpinner.setAdapter(adapter);
        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedCell.setBackgroundColor(stringToColor(listColors.get(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });
    }

    private int stringToColor(String str){
        if (str.equals(getString(R.string.blue))){
            return Color.parseColor("#2196F3");
        } else if(str.equals(getString(R.string.green))){
            return Color.parseColor("#8BC34A");
        } else if(str.equals(getString(R.string.yellow))){
            return Color.parseColor("#FFEB3B");
        } else {
            return Color.parseColor("#EEEEEE");
        }
    }
}

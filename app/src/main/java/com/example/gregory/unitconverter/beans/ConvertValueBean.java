package com.example.gregory.unitconverter.beans;

import java.io.Serializable;

/**
 * Created by Gregory on 12/12/2016.
 */

public class ConvertValueBean implements Serializable{
    private boolean show = true;
    private String uom; //unite de mesure
    private String baseValue; //valeur sur laquelle la conversion de base
    private double convertedValueForOneBaseValue; //valeur convertit pour 1 unite de mesure de base
    private double convertedValue = 0;

    public ConvertValueBean(String uom, String baseValue, double convertedValueForOneBaseValue) {
        this.uom = uom;
        this.baseValue = baseValue;
        this.convertedValueForOneBaseValue = convertedValueForOneBaseValue;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(String baseValue) {
        this.baseValue = baseValue;
    }

    public double getConvertedValueForOneBaseValue() {
        return convertedValueForOneBaseValue;
    }

    public void setConvertedValueForOneBaseValue(double convertedValueForOneBaseValue) {
        this.convertedValueForOneBaseValue = convertedValueForOneBaseValue;
    }

    public double getConvertedValue() {
        return convertedValue;
    }

    public void setConvertedValue(double convertedValue) {
        this.convertedValue = convertedValue;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }
}

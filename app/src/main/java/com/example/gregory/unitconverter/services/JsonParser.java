package com.example.gregory.unitconverter.services;

import com.example.gregory.unitconverter.beans.ConvertValueBean;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gregory on 2016-12-12.
 */

public class JsonParser {
    public List<ConvertValueBean> parseRates(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject rates = jsonObject.getJSONObject("rates");
            List<ConvertValueBean> listConvert = new ArrayList<>();
            for(int i = 0; i < rates.names().length(); i++){
                double d = Double.parseDouble(rates.get(rates.names().get(i).toString()).toString());
                listConvert.add(new ConvertValueBean(rates.names().get(i).toString(), "USD", d));
            }
            listConvert.add(new ConvertValueBean("USD", "USD", 1));
            return listConvert;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

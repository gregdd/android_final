package com.example.gregory.unitconverter.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.gregory.unitconverter.R;
import com.example.gregory.unitconverter.beans.ConvertValueBean;

import java.text.DecimalFormat;

/**
 * Created by Gregory on 12/12/2016.
 */

public class CustomListConverterAdapter extends ArrayAdapter<ConvertValueBean> {
    Context context;
    public CustomListConverterAdapter(Context context){
        super(context, R.layout.item_list_row);
        this.context = context;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ConvertValueBean convertValueBean = getItem(position);

        View v = convertView;
        if (v == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = layoutInflater.inflate(R.layout.item_list_row, parent, false);
        }
        TextView value = (TextView) v.findViewById(R.id.convertedvalue);
        TextView uof = (TextView) v.findViewById(R.id.uom);

        value.setText(new DecimalFormat("#.#####").format(convertValueBean.getConvertedValue()));
        uof.setText(convertValueBean.getUom());
        return v;

    }
}

package com.example.gregory.unitconverter.api;

/**
 * Created by Gregory on 2016-12-12.
 */

public interface FetchInterface {
    void onResponse(String output);
}
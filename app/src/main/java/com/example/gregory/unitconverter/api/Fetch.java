package com.example.gregory.unitconverter.api;

import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Gregory on 2016-12-12.
 */

public class Fetch extends AsyncTask<Void, Void, String> {
    private FetchInterface fetchInterface;


    public Fetch(FetchInterface fetchInterface){
        this.fetchInterface = fetchInterface;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            URL url = new URL("http://api.fixer.io/latest?base=USD");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return new String(out.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String output){
        super.onPostExecute(output);
        fetchInterface.onResponse(output);
    }
}
